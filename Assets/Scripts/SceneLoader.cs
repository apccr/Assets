﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {
	public int SCNumber;

 public void Executor (){
		StartCoroutine(LoadScene());
	}


	public void LoadSceneNum(int num) {
				if (num<0 || num >= SceneManager.sceneCountInBuildSettings){
			Debug.LogWarning("Can't load scene num"+ num +",Scene Manager only  has" + SceneManager.sceneCountInBuildSettings + "scenes in  BuildSettings!");
			return;
		}
		LoadingScreenManager.LoadScene(num);
	}

	IEnumerator LoadScene () {

		yield return new WaitForSeconds(1);
		LoadSceneNum (SCNumber);

	}
}