﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDamage : MonoBehaviour {
	public GameObject Player1Health;
	public GameObject Player2Health;
	public GameObject Player3Health;
	public GameObject Player4Health;
	public float damage= 0.05f;

	// Use this for initialization
	void Start () {
		
	}

	void OnTriggerEnter (Collider collider) {
		if (collider.tag=="Player1") {
			Player1Health.GetComponent<Image>().fillAmount-=damage;
			}
		if (collider.tag=="Player2") {
			Player2Health.GetComponent<Image>().fillAmount-=damage;
		}
		if (collider.tag=="Player3") {
			Player3Health.GetComponent<Image>().fillAmount-=damage;
		}
		if (collider.tag=="Player4") {
			Player4Health.GetComponent<Image>().fillAmount-=damage;
		}
	}

}
