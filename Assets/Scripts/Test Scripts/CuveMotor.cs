﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuveMotor : MonoBehaviour {

	public int PlayerNumber = 1;
	// Contador de Saltos 
	public float JumpCount = 0; 
	//Maximo de saltos multiples
	public float JumpMAX = 2; 
	// Velocidad del movimiento en el eje X
	public float HorizontalVelocity = 10.0f ; 
	// fuerza de gravedad en el eje Y
	public float GravityForce = 10.0f ; 

	private Rigidbody rb;
	//La fuerza con la que se efectua el salto
	public float jumpforce =10.0f;
	public GameObject Character;
	//Busca el objeto con el componente Floor Detection y seleccionalo para que funcione el salto multiple
	public GameObject FD;

	// Use this for initialization
	void Start () {
		// Busca el Rigidbody del Character para efectuar el movimiento
		rb = Character.GetComponent<Rigidbody>();
	}
	


	// Update es llamado una vez por frame
	void Update () {	

		if (Input.GetButtonDown ("AP" + PlayerNumber) && JumpCount < JumpMAX) {
			Jump ();
		}
		if ( JumpCount > 1 ) {
			var y = Time.deltaTime * GravityForce;
			transform.Translate(0, -y, 0);
		}
		// Controllador 2D  eje "x"
		var x = Input.GetAxis("HorizontalP"+ PlayerNumber) * Time.deltaTime * HorizontalVelocity;
		transform.Translate(x, 0, 0);

		//Detección de Colisión con el "Suelo"
		JumpCount=FD.GetComponent<FloorDetector>().jc;
	}


	public void Atack() {
		
	}

	//Función de salto
	public void Jump() {	
		rb.velocity = new Vector3 (0,  jumpforce, 0);
		FD.GetComponent<FloorDetector>().jc += 1;	
	}


}
