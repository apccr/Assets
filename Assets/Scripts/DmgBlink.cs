﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DmgBlink : MonoBehaviour {
	public float fadeDirection = -1;
	//The speed which the fading process will be performed
	public float fadeSpeed = 0.3f;
	//The initial alpha for the texture
	public float alpha = 1.0f;
	public float health ;
	public GameObject HealthBar;
	public AudioSource hit;
	public AudioSource dying;
	Renderer rend;
	public Texture blink, main;
	public GameObject FadeIn;
	public float FadeInAlpha;

	// Use this for initialization
	void Start () {
		
		rend = GetComponent<Renderer>();
	
	}
	void Update () {
		FadeInAlpha = FadeIn.GetComponent<Fadein> ().alpha;
		health = HealthBar.GetComponent<Image> ().fillAmount;
		if (FadeInAlpha == 0 && health <0.10 && alpha == 1) {
			StartCoroutine(BlinkDying());
			alpha = 0;
		}

	
	}
	void OnGUI()
	{
			alpha -= fadeDirection * fadeSpeed * Time.deltaTime;// Time.deltaTime;
			alpha = Mathf.Clamp01 (alpha);


	}
	public bool AreWeDone()
	{
		if (fadeDirection == -1 && alpha == 0)
		{
			return true;
		}
		else if (fadeDirection == 1 && alpha == 1)
		{
			return true;
		}
		return false;
	}
	public void fadeIn()
	{
		fadeDirection = -1;
	}


	public void fadeOut()
	{
		fadeDirection = 1;
	}

	// Update is called once per frame
	void OnTriggerEnter (Collider other) {
		if (other.tag == "Enemy") 
		{
			hit.Play();		
		StartCoroutine(Blink());
		}}

	IEnumerator Blink () {

		rend.material.mainTexture = blink;

		yield return new WaitForSeconds(0.2f);

		rend.material.mainTexture = main;
	}
	IEnumerator BlinkDying () {

		rend.material.mainTexture = blink;
		dying.Play();		

		yield return new WaitForSeconds(fadeSpeed);

		rend.material.mainTexture = main;
	}
}
