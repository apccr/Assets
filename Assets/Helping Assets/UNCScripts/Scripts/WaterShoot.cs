﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterShoot : MonoBehaviour {
	public GameObject WaterBar;
	public float watermeter;
	public float speed;
	public ParticleSystem ParticleLauncher;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		watermeter=WaterBar.GetComponent<GunCtrl> ().waterbar;
		if (Input.GetMouseButton(0) && watermeter >0.00001 && watermeter <1) {
			ParticleLauncher.Emit (10);		

		} 
		
	}
}
