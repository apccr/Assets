﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootNReload : MonoBehaviour {
	public Animation anim;
	public AnimationClip reload;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animation>();
		anim ["Reload"].blendMode=AnimationBlendMode.Additive;

		
		
	}

	void OnTriggerEnter (Collider other) {
		if (other.tag == "Water") {			
			if (Input.GetKey("r")) {
				anim.Play ("Reload");

			}
			if (Input.GetKeyUp("r")) {
				anim.Stop ("Reload");
			}
		}
			

		}
		
	// Update is called once per frame
	void Update () {

		
	}
}
