﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastDemoScript : MonoBehaviour {

	//The fade direction, -1 means from texture to no texture, 1 means the opposite
	public float timeDirection = -1;
	//The speed which the fading process will be performed
	public float timeSpeed = 0.3f;
	//The initial alpha for the texture
	public float time = 1.0f;

	public GameObject Camera;



	// Use this for initialization
	void Start() {
		timeSpeed=Camera.GetComponent<FadeOut1> ().fadeSpeed;
		Camera.GetComponent<FadeOut1> ().enabled = true;

	}
	// Update is called once per frame
	void Update () {
		time -= timeDirection * timeSpeed * Time.deltaTime;// Time.deltaTime;
		time = Mathf.Clamp01 (time);

		if (time == 1) {
			LoadTutorial ();

		}

	}
	void LoadTutorial(){
		Application.LoadLevel ("Last Demo Scene");
	}

}