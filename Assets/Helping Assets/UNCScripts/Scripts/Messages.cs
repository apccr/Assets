﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Messages : MonoBehaviour {

	TypeWriterEffect text;
	public GameObject TypeWritedText;
	Image alert ;
	public GameObject Message;
	public bool IsLightOn = false;
	public bool Showmessage = false ;

	void Start()
		{
		GameObject Text = TypeWritedText;
		text =  Text.GetComponent<TypeWriterEffect>();
		text.enabled = false;

		GameObject warning = GameObject.FindWithTag ("Alert");
		alert =  warning.GetComponent<Image>();
		alert.enabled = false;
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Player") {
			AlertIcon ();
			IsLightOn = true;
				}

	}
	void Update()
	{ 
		if (Showmessage == false) {
			Message.SetActive (false);
		}
		else {
				Message.SetActive(true);
			}		
		
	}

	void OnTriggerStay (Collider other)
	{
		if (other.tag == "Player") 
		{			
			if (IsLightOn == true && Input.GetKeyUp("space")) {
				ShowBox ();
				TEXT ();
		}
		}
	}

	void OnTriggerExit (Collider other)
	{
		if (other.tag == "Player") {
			AlertIcon ();
			UnShowBox ();
			UnShowTEXT () ;
}
	}

	public void AlertIcon()
	{
		alert.enabled = !alert.enabled;
	}
	public void TEXT()
	{
		text.enabled = !text.enabled;
	}
	public void UnShowTEXT()
	{
		text.enabled = false;
	}

	public void ShowBox ()
	{
		Showmessage = !Showmessage;
	
	}

	public void UnShowBox ()
	{
		Showmessage = false;
		IsLightOn = false; 

	

	}
}


