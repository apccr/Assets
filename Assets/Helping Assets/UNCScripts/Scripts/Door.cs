﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Door : MonoBehaviour {
	public GameObject GearDoor;
	public GameObject LastDoorMessage;
	public GameObject CurrentObj;
	Image alert ;

	void Start()
	{
		GameObject warning = GameObject.FindWithTag ("Alert");
		alert =  warning.GetComponent<Image>();
		alert.enabled = false;


	}
	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Player") {
			AlertIcon ();			
		}
	}


	void OnTriggerStay (Collider other) {
		if (Input.GetKeyUp("space")) {
			GearDoor.SetActive (true);
			LastDoorMessage.SetActive(true);
			Destroy (CurrentObj, 0.001f);

		}

	}
	void OnTriggerExit (Collider other)
	{
		if (other.tag == "Player") {
			AlertIcon ();
		}
	}
	public void AlertIcon()
	{
		alert.enabled = !alert.enabled;
	}
}
