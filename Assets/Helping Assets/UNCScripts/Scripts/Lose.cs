﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Lose : MonoBehaviour {


	public GameObject healthbar;
	public GameObject Camera;
	Image life;
	bool camera;
		// Use this for initialization
		void Start()
		{
		Camera.GetComponent<FadeOut>().enabled=false;
	}

		// Update is called once per frame
		void Update()
		{
		if (healthbar.GetComponent<Image>().fillAmount == 0) {
			Camera.GetComponent<FadeOut>().enabled=true;	
			
		}

		if (Camera.GetComponent<FadeOut>().alpha== 1) {
			DeadScreen();

		}

	}
	void DeadScreen(){
		Application.LoadLevel ("DeadScreen");
	}

}
