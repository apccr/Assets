﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour {
	public GameObject GateText ;
	public GameObject GateText2 ;
	public GameObject SecondRound;

	void OnTriggerEnter (Collider other) {
		if (other.tag == "Player") {
			GameVariables.keyCount+=2;
			Destroy (gameObject);
			Destroy (GateText);
			GateText2.SetActive(true);
			SecondRound.SetActive(true);

		}
		
	}
	

}
