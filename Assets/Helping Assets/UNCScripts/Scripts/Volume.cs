﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Volume : MonoBehaviour {

	public AudioSource Musica_01;
	public float SoundLevel= 5;
	void OnTriggerStay (Collider other) {
		if (other.tag == "Player"){
			Musica_01.volume -= SoundLevel *Time.deltaTime;
			}
		
	}
}
