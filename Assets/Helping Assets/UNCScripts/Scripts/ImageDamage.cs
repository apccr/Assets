﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageDamage : MonoBehaviour {
	public Image Health ;
	public  float  DamageSpeed = 5 ;
	public static Color white;
	public GameObject Player;
	// Use this for initialization
	void Start () {
		
		
	}
	
	// Update is called once per frame
	void OnCollisionEnter (Collision other)
	{
		if (other.gameObject == Player) {
			Health.color = Color.Lerp (Health.color, Color.white, DamageSpeed * Time.deltaTime);
		} else {
			Health.color = Color.Lerp (Health.color, Color.clear, DamageSpeed * Time.deltaTime);
		}
	}
		void OnCollisionExit (Collision other){
		if (other.gameObject == Player) {
			Health.color = Color.Lerp (Health.color, Color.clear, DamageSpeed * Time.deltaTime);
		}
		}


}
