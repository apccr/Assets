﻿using UnityEngine;
using System.Collections;

public class chase : MonoBehaviour {
	
	[SerializeField]
	private Transform enemy;  //Enemy that contains its own animator controller
	[SerializeField]
	private Transform player;  //Target our gameObject will follow
	[SerializeField]
	private float trackingDistance = 30.0f; //How far ahead our gameObject can see 
	[SerializeField]
	private float lookSpeed = 5.0f; //How fast our gameObject will rotate
	[SerializeField]
	private float stopDistance = 3.0f; //How far from our Target will our gameObject stop
	[SerializeField]
	private float moveSpeed = 0.05f; //How fast our gameObject can move
	[SerializeField]
	private float lookAngle = 30.0f; //The radius our gameObject is able to see
	private Animator anim; //We will need the Animator component attached to our gameObject // Use this for initialization
	void Start () {
		//Grab the Animator attached to our gameObject
		anim =enemy.GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () {
		//Find the direction we wish to look at
		Vector3 direction = player.position - this.transform.position;
		//Find the angle of our gameObject
		float angle = Vector3.Angle(direction, this.transform.forward);
		//If the distance to our Target is less than our trackingDistance
		if (Vector3.Distance(player.position, this.transform.position) < trackingDistance /* && angle < lookAngle */)
		{
			//Freeze the y axis to prevent our gameObject from moving vertically
			direction.y = 0;
			//Turn our gameObject to look at our target
			this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), lookSpeed * Time.deltaTime);
			//If our gameObject is farther than our designated stopDistance
			anim.SetBool ("isIdle", false);
			if(direction.magnitude > stopDistance)
			{
				//Move our gameObject towards our Target, until we reach our stopDistance
				this.transform.Translate(0, 0, moveSpeed);
				//Set our Walking parameter on our Animator to true, plays our walking animation
				anim.SetBool("isWalking", true);
				anim.SetBool("isAtacking",false);
			}
			else
			{
				//Set our Walking parameter on our Animator to false, plays our default Animator state(idle)
				anim.SetBool("isWalking", false);
				anim.SetBool("isAtacking",true);
			}
		}
	}
}﻿
