﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastExitMessage : MonoBehaviour {
	public GameObject thismessage;
	public GameObject Onlastmessage;


	// Cuando no es el ultimo mensage pon el mismo texto, si no, pon el Canvas
	void Update () {
		if (Input.GetKeyDown("space")) {
			Destroy (thismessage,2);
			Destroy (Onlastmessage,2);
			GetComponent<LastDemoScript>().enabled=true;
		
		}
	}
}
