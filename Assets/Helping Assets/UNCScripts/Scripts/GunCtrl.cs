﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunCtrl : MonoBehaviour {

	// Deactivate Gun Null
	public float waterbar; 
	public float speed;
	public GameObject WaterBar;
	public GameObject Gun;
	Image Water;
	void Start (){
		Cursor.visible = true;
	}

	// Update is called once per frame
	void Update () {

		waterbar = Mathf.Clamp(waterbar,0,1);
		WaterBar.GetComponent<Image>().fillAmount=waterbar;
		if (Input.GetMouseButton(0)) {
			waterbar -= speed * Time.deltaTime;
		}

		if (waterbar < 0) {
			Gun.SetActive(false);
			}
		if (waterbar > -0.1) {
			Gun.SetActive(true);
		}

		}
}
